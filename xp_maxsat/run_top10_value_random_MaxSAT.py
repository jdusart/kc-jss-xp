#!/usr/bin/env python

import os
import io
import sys
import random
import math
from threading import Timer
import json
import time, subprocess

def save_dict(dic,path,indent=4):
    with open(path, 'w') as fp:
        json.dump(dic, fp, indent=indent)
        fp.close()

def load_dict(path):
    if os.path.exists(path):
        with open(path, 'r') as fp:
            return json.load(fp)
            fp.close()
    return {}

def runMaxHS(file_in, timeout):
    timeStarted = time.time()
    process = subprocess.Popen(["./prog/maxhs","-printSoln",file_in], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    timer = Timer(timeout, process.terminate)
    try:
        res = "UNSATISFIABLE"
        sol = ""
        timer.start()
        for line in io.TextIOWrapper(process.stdout):
            if line.startswith("s "):
                res = line.split("s ")[1].split("\n")[0]
            if line.startswith("v "):
                sol = line
        process.terminate()
        process.stdout.close()
        process.stderr.close()
        delta = time.time()-timeStarted
        if res == "UNSATISFIABLE":
            return (delta,[])
        if res == "UNKNOWN":
            return (delta,"UNKNOWN")
        
        sol = sol.split("v ")[1].split("\n")[0]
        sol = [x+1 for x in range(0,len(sol)) if sol[x]=="1"]+[-(x+1) for x in range(0,len(sol)) if sol[x]=="0"]
        sol = [" ".join([str(x) for x in sol])]
        delta = time.time()-timeStarted
        return (delta,sol)
    except Exception as e:
        print("Exception: %s" % e)
        process.terminate()
        process.stdout.close()
        process.stderr.close()
        process.wait()
        return (time.time()-timeStarted,"NaN")



    
solvers = {"maxhs":runMaxHS}

def generate_weights_for_computation(nbvar,weights,limit):
    res = {x:v for x,v in weights.items()}
    for i in range(1,nbvar+1):
        if i not in res:
            res[i]=random.randint(1,limit)
        if -i not in res:
            res[-i]=random.randint(1,limit)

    return res


def create_min_wcnf(nbvar,hard_clauses,output_file,weights):
    top = math.ceil(sum(weights.values()))+1

    new_clauses =  ["%d %s\n" %(top,x.strip()) for x in hard_clauses if "0" in x]
    new_clauses = ["%d %d 0\n" % (weights[x],x) for x in weights] + new_clauses

    new_command = "p wcnf %d %d %d" % (nbvar,len(new_clauses),top)    
    with open(output_file,'w') as f:
        f.write("%s\n" % new_command)
        for x in new_clauses:
            f.write("%s" % x)
    
    return output_file

def sol_to_clause(sol):
    children = set()
    for s in sol:
        for v in s.split(" "):
            if v!="0":
                if v[0]=="-":
                    children.add(v[1:])
                else:
                    children.add("-%s" % v)
    return " ".join(children)


def eval_sol(sol,weights):
    return sum([weights[int(x)] for x in sol.split(' ')])

benchmark = sys.argv[1]

for solver in solvers:
    for limit in [1,100,10000,1000000]:
        stat_data_path = os.path.join("out","top10_value_%s_%d" % (solver,limit))
        if not os.path.exists(stat_data_path):
            os.makedirs(stat_data_path)


        for filename in [x.split('.cnf')[0] for x in os.listdir(benchmark)]:
            print("Solving %s/%s with solver %s" % (benchmark,filename,solver))
            model = "%s/%s.cnf" % (benchmark,filename)
            tmp_file = "tmp/tmp_maxsat_%s.wcnf" % filename

            data = {}
            if not os.path.exists(os.path.join(stat_data_path,"%s.json" % (filename))):
                for ite in range(5):
                    timeout = 10*60
                    timeStarted = time.time()
                        
                    sentence = open(model).readlines()
                    sentence = [x for x in sentence if not x.startswith('c')]
                    problem = [x for x in sentence if x.startswith('p')][0]
                    var = int(problem.split(" ")[2])
                    clauses = [x for x in sentence if not x.startswith('p')]
                    weights = generate_weights_for_computation(var,{},limit)

                    data[ite] = []
                    last_value = -1
                    i=0
                    while (i<10):
                        create_min_wcnf(var,clauses,tmp_file,weights)
                        (delta,sol) = runMaxHS(tmp_file,timeout)
                        timeout = timeout-delta
                        
                        if sol == "UNKNOWN":
                            data[ite].append("UNKNOWN")
                            break
                        if sol == "NaN":
                            data[ite].append("NaN")
                            break
                        if timeout <= 0:
                            data[ite].append("TimeOut")
                            break
                        if len(sol)==0:
                            data[ite].append(time.time()-timeStarted)
                            data[ite].append("UNSAT")
                            break
                        if len(sol)!=0:
                            model_found = sol_to_clause(sol)
                            clauses.append("%s 0\n" % model_found)
                            
                            model_weight = eval_sol(model_found,weights)

                            if model_weight != last_value:
                                data[ite].append(time.time()-timeStarted)
                                i+=1
                                last_value = model_weight

                    os.remove(tmp_file)
                save_dict(data,os.path.join(stat_data_path,"%s.json" % (filename)))


