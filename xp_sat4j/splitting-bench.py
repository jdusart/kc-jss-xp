#!/usr/bin/env python
# coding: utf-8

# # Multithread prototype

# In[1]:


import os
import shutil
import random
import sys

nb = int(sys.argv[1])

files = list(os.listdir("../Benchmark"))
random.shuffle(files)

for i,file in enumerate(files):
    directory = os.path.join("tmp","Benchmark-%d" % (i%nb))
    if not os.path.exists(directory):
        os.makedirs(directory)
    shutil.copy2(os.path.join("..","Benchmark",file),os.path.join(directory,file))
    
def create_script(script_name,data_dirs,script):
    with open(script_name,"w") as f:
        f.write("#!/bin/sh\n\n")
        for directory in data_dirs:
            f.write("mkdir -p %s\n" % directory)
        f.write("\n")
        for i in range(nb):
            f.write("python3 %s tmp/Benchmark-%d &\n" % (script,i))
        f.write("\nwait\n")
    return
    

#create_script("run_sat4j_topsol10.sh",["out/top10_config_sat4j_1","out/top10_config_sat4j_100","out/top10_config_sat4j_10000",
#                                       "out/top10_config_sat4j_1000000"],"run_top10_config_random_sat4j.py")
                                       
#create_script("run_sat4j_topval10.sh",["out/top10_value_sat4j_1","out/top10_value_sat4j_100","out/top10_value_sat4j_10000",
#                                       "out/top10_value_sat4j_1000000"],"run_top10_value_random_sat4j.py")



