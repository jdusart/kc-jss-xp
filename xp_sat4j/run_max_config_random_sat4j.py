#!/usr/bin/env python
# coding: utf-8

# # Multithread prototype

# In[1]:


import os
import subprocess
import json
import math
import random
import argparse


def save_dict(dic,path,indent=4):
    with open(path, 'w') as fp:
        json.dump(dic, fp, indent=indent)

def load_dict(path):
    if os.path.exists(path):
        with open(path, 'r') as fp:
            return json.load(fp)
    return {}

def computeStats(file_in,limit,timeout,max_nb_models):
    # 
    process = subprocess.Popen(["java","-Xmx12228m","-cp","prog/m2/repository/org/hamcrest/hamcrest-core/1.1/hamcrest-core-1.1.jar:prog/m2/repository/org/antlr/antlr4-runtime/4.7.1/antlr4-runtime-4.7.1.jar:prog/m2/repository/io/github/myui/btree4j/0.9.1/btree4j-0.9.1.jar:prog/m2/repository/log4j/log4j/1.2.17/log4j-1.2.17.jar:prog/m2/repository/commons-cli/commons-cli/1.2/commons-cli-1.2.jar:prog/m2/repository/commons-logging/commons-logging/1.0.4/commons-logging-1.0.4.jar:prog/m2/repository/org/ow2/sat4j/org.ow2.sat4j.maxsat/2.3.6/org.ow2.sat4j.maxsat-2.3.6.jar:prog/m2/repository/org/ow2/sat4j/org.ow2.sat4j.core/2.3.6/org.ow2.sat4j.core-2.3.6.jar:prog/m2/repository/org/ow2/sat4j/org.ow2.sat4j.sat/2.3.6/org.ow2.sat4j.sat-2.3.6.jar:prog/m2/repository/commons-beanutils/commons-beanutils/1.8.3/commons-beanutils-1.8.3.jar:prog/m2/repository/net/sf/jchart2d/jchart2d/3.3.2/jchart2d-3.3.2.jar:prog/m2/repository/org/apache/xmlgraphics/xmlgraphics-commons/1.3.1/xmlgraphics-commons-1.3.1.jar:prog/m2/repository/commons-io/commons-io/1.3.1/commons-io-1.3.1.jar:prog/m2/repository/com/jidesoft/jide-oss/2.4.8/jide-oss-2.4.8.jar:prog/m2/repository/org/ow2/sat4j/org.ow2.sat4j.pb/2.3.6/org.ow2.sat4j.pb-2.3.6.jar:prog/WINSTON-0.1-jss-alpha.jar","xps.jss.sat4j.EnumOptModelsLimits",file_in,str(limit),str(timeout),str(max_nb_models)], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    try:  
        stdout, stderr = process.communicate()
        out = stdout.decode()
        # stats = [ float(x.split("\n")[0].split("time: ")[1]) for i,x in enumerate(out.split("Model ")[1:]) ]
        stats = {}
        stats["#models"] = int(out.split("#models found: ")[1].split('\n')[0])
        stats["time"] = int(out.split("Total time: ")[1].split('\n')[0])
        # stats["total"] = float(out.split("Total time: ")[1].split('\n')[0])
        return ("Success",stats)    
    except Exception as e:
        process.terminate()
        stdout, stderr = process.communicate()
        out = stdout.decode()
        print(out)
        print("Error with model %s and limit %d: %s" % (file_in,limit,str(e)))
        #out = stdout.decode()
        #stats = {i:float(x.split("\n")[0].split("time: ")[1]) for i,x in enumerate(out.split("Model ")[1:])}
        #stats["start"] = float(out.split("Start iterating: ")[1].split('\n')[0])
        return ("Timeout",stats)    





for limit in [100,10000,1000000]:
    stat_data_path = os.path.join("out","limit_config_sat4j_%d" % (limit))
    if not os.path.exists(stat_data_path):
        os.makedirs(stat_data_path)
    
    for filename in ["LargeAutomotive","2.6.28.6"]:
        print("Solving %s with solver %s and limit %d" % (filename,"sat4j",limit))
        if not os.path.exists(os.path.join(stat_data_path,"%s.json" % (filename))):
            data = {}
            for ite in range(5):
                data[ite] = computeStats(os.path.join('../Benchmark',"%s.cnf" % filename),limit,10*60,10000000)
            save_dict(data,os.path.join(stat_data_path,"%s.json" % (filename)))
    
    
    
    
