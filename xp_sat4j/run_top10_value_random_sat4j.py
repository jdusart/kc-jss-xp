#!/usr/bin/env python
# coding: utf-8

# # Multithread prototype

# In[1]:


import os
import sys
import subprocess
import json
import math
import random
import argparse


def save_dict(dic,path,indent=4):
    with open(path, 'w') as fp:
        json.dump(dic, fp, indent=indent)

def load_dict(path):
    if os.path.exists(path):
        with open(path, 'r') as fp:
            return json.load(fp)
    return {}

def computeStats(file_in,limit,timeout=60*10):
    process = subprocess.Popen(["java","-Xmx12228m","-cp","prog/m2/repository/org/hamcrest/hamcrest-core/1.1/hamcrest-core-1.1.jar:prog/m2/repository/org/antlr/antlr4-runtime/4.7.1/antlr4-runtime-4.7.1.jar:prog/m2/repository/io/github/myui/btree4j/0.9.1/btree4j-0.9.1.jar:prog/m2/repository/log4j/log4j/1.2.17/log4j-1.2.17.jar:prog/m2/repository/commons-cli/commons-cli/1.2/commons-cli-1.2.jar:prog/m2/repository/commons-logging/commons-logging/1.0.4/commons-logging-1.0.4.jar:prog/m2/repository/org/ow2/sat4j/org.ow2.sat4j.maxsat/2.3.6/org.ow2.sat4j.maxsat-2.3.6.jar:prog/m2/repository/org/ow2/sat4j/org.ow2.sat4j.core/2.3.6/org.ow2.sat4j.core-2.3.6.jar:prog/m2/repository/org/ow2/sat4j/org.ow2.sat4j.sat/2.3.6/org.ow2.sat4j.sat-2.3.6.jar:prog/m2/repository/commons-beanutils/commons-beanutils/1.8.3/commons-beanutils-1.8.3.jar:prog/m2/repository/net/sf/jchart2d/jchart2d/3.3.2/jchart2d-3.3.2.jar:prog/m2/repository/org/apache/xmlgraphics/xmlgraphics-commons/1.3.1/xmlgraphics-commons-1.3.1.jar:prog/m2/repository/commons-io/commons-io/1.3.1/commons-io-1.3.1.jar:prog/m2/repository/com/jidesoft/jide-oss/2.4.8/jide-oss-2.4.8.jar:prog/m2/repository/org/ow2/sat4j/org.ow2.sat4j.pb/2.3.6/org.ow2.sat4j.pb-2.3.6.jar:prog/WINSTON-0.1-jss-alpha.jar","xps.jss.sat4j.Enum10OptValues",file_in,str(limit)], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    try:  
        process.wait(timeout=timeout)
        stdout, stderr = process.communicate()
        out = stdout.decode()
        stats = [ float(x.split("\n")[0].split("time: ")[1]) for i,x in enumerate(out.split("Value ")[1:]) ]
        # stats["start"] = float(out.split("Start iterating: ")[1].split('\n')[0])
        # stats["total"] = float(out.split("Total time: ")[1].split('\n')[0])
        return stats    
    except Exception as e:
        process.terminate()
        stdout, stderr = process.communicate()
        print("Error with model %s and limit %d: %s" % (file_in,limit,str(e)))
        #stats = {i:float(x.split("\n")[0].split("time: ")[1]) for i,x in enumerate(out.split("Model ")[1:])}
        #stats["start"] = float(out.split("Start iterating: ")[1].split('\n')[0])
        return []    


benchmark = sys.argv[1]

for limit in [1,100,10000,1000000]:
    stat_data_path = os.path.join("out","top10_value_%s_%d" % ("sat4j",limit))
    if not os.path.exists(stat_data_path):
        os.makedirs(stat_data_path)

    for filename in [x.split('.cnf')[0] for x in os.listdir(benchmark)]:
        if not os.path.exists(os.path.join(stat_data_path,"%s.json" % (filename))):
            print("Solving %s with solver %s" % (filename,"sat4j"))
            data = {}
            for ite in range(5):
                data[ite] = computeStats(os.path.join(benchmark,"%s.cnf" % filename),limit)
            save_dict(data,os.path.join(stat_data_path,"%s.json" % (filename)))
    
    
    
    
