#!/usr/bin/env python

import sys
import os
import random
import json
import time, subprocess

def save_dict(dic,path,indent=4):
    with open(path, 'w') as fp:
        json.dump(dic, fp, indent=indent)

def load_dict(path):
    if os.path.exists(path):
        with open(path, 'r') as fp:
            return json.load(fp)
    return {}



def solve(file_in,solver,timeout=700):
    timeStarted = time.time()
    process = subprocess.Popen(["time",solver,file_in], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    try:  
        process.wait(timeout=timeout)
        stdout, stderr = process.communicate()
        out = stdout.decode()
        delta = time.time()-timeStarted
        result = out.split("\ns ")[1].split("\n")[0]
        sol = [x.split('\n')[0] for x in out.split("\nv ")[1:]]
        return (delta,result,sol)
    except subprocess.TimeoutExpired:
        process.terminate()
        stdout, stderr = process.communicate()
        print("%s:\n%s" % (file_in,stderr.decode()))
        process.wait()
        return ("NaN","NaN",[])

    
solvers = {"cadical":"./prog/cadical-alluip/bin/cadical"}


def sol_to_clause(sol):
    children = set()
    for s in sol:
        for v in s.split(" "):
            if v!="0":
                if v[0]=="-":
                    children.add(v[1:])
                else:
                    children.add("-%s" % v)
    return " ".join(children)

benchmark = sys.argv[1]

for solver in solvers:
    stat_data_path = os.path.join("out","%s_count_stats" % solver)
    if not os.path.exists(stat_data_path):
        os.makedirs(stat_data_path)


    for filename in [x.split('.cnf')[0] for x in os.listdir(benchmark)]:
        if not os.path.exists(os.path.join(stat_data_path,"%s.json" % filename)):
            print("Solving %s with solver %s" % (filename,solver))
            data = {}
            timeStarted = time.time()

            model = os.path.join(benchmark,"%s.cnf" % filename)
            tmp_file = "out/tmp2_%s.cnf" % filename

            sentence = open(model).readlines()
            sentence = [x for x in sentence if not x.startswith('c')]
            problem = [x for x in sentence if x.startswith('p')][0]
            var = problem.split(" ")[2]
            clauses = ["%s\n" % x.strip() for x in sentence if not x.startswith('p')]

            timeout = 10*60
            time_start = time.time()
            time_left = timeout
            
            result = "SATISFIABLE"
            count = 0
            while result == "SATISFIABLE" :
                with open(tmp_file,"w") as f:
                    f.write("p cnf %s %d\n" % (var,len(clauses)))
                    f.writelines(clauses)
                
                time_left = timeout - (time.time()-timeStarted)

                (delta,result,sol) = solve(tmp_file,solvers[solver],time_left)
                if result == "NaN" or result == "UNSATISFIABLE":
                    data["count"] = result
                    data["total time"] = time.time()-timeStarted
                    break
                count+=1
                clauses.append("%s 0\n" % sol_to_clause(sol))

                                   
            os.remove(tmp_file)
            save_dict(data,os.path.join(stat_data_path,"%s.json" % filename))


