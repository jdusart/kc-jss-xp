#!/usr/bin/env python
# coding: utf-8

# # Multithread prototype

# In[1]:


import os
import shutil
import random
import sys

nb = int(sys.argv[1])

files = list(os.listdir("../Benchmark"))
random.shuffle(files)

for i,file in enumerate(files):
    directory = os.path.join("tmp","Benchmark-%d" % (i%nb))
    if not os.path.exists(directory):
        os.makedirs(directory)
    shutil.copy2(os.path.join("..","Benchmark",file),os.path.join(directory,file))
    
def create_script(script_name,data_dirs,script):
    with open(script_name,"w") as f:
        f.write("#!/bin/sh\n\n")
        for directory in data_dirs:
            f.write("mkdir -p %s\n" % directory)
        f.write("\n")
        for i in range(nb):
            f.write("python3 %s tmp/Benchmark-%d &\n" % (script,i))
        f.write("\nwait\n")
    return
    

create_script("run_sat_enum.sh",["out/cadical_enum_10_stats"],"run_SAT_enum_10.py")
create_script("run_sat_count.sh",["out/cadical_count_stats"],"run_SAT_count.py")


