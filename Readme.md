# README
----------------------------------------------------

## Requirements:

The experiments were run on a debian linux distribution with java 11.0 and python 3.10.
The analysis requires jupyter-lab with pandas and matplotlib.




# dDNNF experiments

The repository allready contains a version of Winston which is available at https://gitlab.inria.fr/jdusart/winston.

## Compilation

To launch the compilation, you have to run the script "xp_ddnnf/compile_d4.py". The dDNNF will be stored in the directory "xp_ddnnf/out/dDNNF" and the compilation stats in the directory "xp_ddnnf/out/d4_compilation_stats".

## Pointing out configuration 1 and 10

You have to run the script "xp_ddnnf/run_satisfiability.py". The results will be stored in "xp_ddnnf/out/point_out_*".

## Sampling

You have to run the script "xp_ddnnf/run_sampling.py". The results will be stored in "xp_ddnnf/out/sampling_stats".

## Counting

You have to run the script "xp_ddnnf/run_counting.py". The results will be stored in "xp_ddnnf/out/counting_stats".

## Top 10 config

You have to run the script "xp_ddnnf/run_top10_config_random_jddnnf.py". The results will be stored in "xp_ddnnf/out/top10_config_jddnnf_*".

## Top 10 values

You have to run the script "xp_ddnnf/run_top10_value_random_jddnnf.py". The results will be stored in "xp_ddnnf/out/top10_value_jddnnf_*".

## Scalability 

To push the top configuration, you have to run the script "xp_ddnnf/run_max_config_jddnnf.py". The results will be stored in "xp_ddnnf/out/max_config_jddnnf".



# MaxSAT experiments

## MaxHS

You need a binary of MaxHS to run the experiments; http://www.maxhs.org/. The binary must be called "maxhs" and put in the directory xp_maxsat/prog/.

## Running experiments

You can just run the script xp_maxsat.sh, which will split the benchmark and run all the experiments.



# Sat4J experiments

## Running experiments

You can just run the script xp_sat4j.sh, which will split the benchmark and run all the experiments.





