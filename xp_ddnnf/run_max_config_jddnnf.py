#!/usr/bin/env python
# coding: utf-8

# # Multithread prototype

# In[1]:


import os
import subprocess
import time
import json
import math
import random
import argparse


def save_dict(dic,path,indent=4):
    with open(path, 'w') as fp:
        json.dump(dic, fp, indent=indent)

def load_dict(path):
    if os.path.exists(path):
        with open(path, 'r') as fp:
            return json.load(fp)
    return {}

def enumOpt(file_in,nb_var,nb,limit):
    timeStarted = time.time()
    process = subprocess.Popen(["java","-Xmx24456m","-cp","prog/m2/repository/org/junit/platform/junit-platform-launcher/1.7.2/junit-platform-launcher-1.7.2.jar:prog/m2/repository/org/apiguardian/apiguardian-api/1.1.0/apiguardian-api-1.1.0.jar:prog/m2/repository/org/junit/platform/junit-platform-engine/1.7.2/junit-platform-engine-1.7.2.jar:prog/m2/repository/org/junit/jupiter/junit-jupiter-api/5.7.1/junit-jupiter-api-5.7.1.jar:prog/m2/repository/org/opentest4j/opentest4j/1.2.0/opentest4j-1.2.0.jar:prog/m2/repository/org/junit/platform/junit-platform-commons/1.7.1/junit-platform-commons-1.7.1.jar:prog/m2/repository/org/junit/jupiter/junit-jupiter-engine/5.7.1/junit-jupiter-engine-5.7.1.jar:prog/m2/repository/org/junit/jupiter/junit-jupiter-params/5.7.1/junit-jupiter-params-5.7.1.jar:prog/m2/repository/com/googlecode/json-simple/json-simple/1.1.1/json-simple-1.1.1.jar:prog/m2/repository/junit/junit/4.10/junit-4.10.jar:prog/m2/repository/org/hamcrest/hamcrest-core/1.1/hamcrest-core-1.1.jar:prog/m2/repository/org/antlr/antlr4-runtime/4.7.1/antlr4-runtime-4.7.1.jar:prog/m2/repository/io/github/myui/btree4j/0.9.1/btree4j-0.9.1.jar:prog/m2/repository/log4j/log4j/1.2.17/log4j-1.2.17.jar:prog/m2/repository/commons-cli/commons-cli/1.2/commons-cli-1.2.jar:prog/m2/repository/commons-logging/commons-logging/1.0.4/commons-logging-1.0.4.jar:prog/m2/repository/com/google/guava/guava/30.1.1-jre/guava-30.1.1-jre.jar:prog/m2/repository/com/google/guava/failureaccess/1.0.1/failureaccess-1.0.1.jar:prog/m2/repository/com/google/guava/listenablefuture/9999.0-empty-to-avoid-conflict-with-guava/listenablefuture-9999.0-empty-to-avoid-conflict-with-guava.jar:prog/m2/repository/com/google/code/findbugs/jsr305/3.0.2/jsr305-3.0.2.jar:prog/m2/repository/org/checkerframework/checker-qual/3.8.0/checker-qual-3.8.0.jar:prog/m2/repository/com/google/errorprone/error_prone_annotations/2.5.1/error_prone_annotations-2.5.1.jar:prog/m2/repository/com/google/j2objc/j2objc-annotations/1.3/j2objc-annotations-1.3.jar:prog/m2/repository/org/cache2k/cache2k-api/2.0.0.Final/cache2k-api-2.0.0.Final.jar:prog/m2/repository/org/cache2k/cache2k-core/2.0.0.Final/cache2k-core-2.0.0.Final.jar:prog/WINSTON-0.1-jss-alpha.jar","xps.mains.TopKConfig",file_in,str(nb_var),str(nb),str(limit)], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = process.communicate()
    try:
        out = stdout.decode()
        stats = {}
        stats["parsing"] = float(out.split("Parsing time: ")[1].split("\n")[0])
        stats["smoothing"] = float(out.split("Smoothing time: ")[1].split("\n")[0])
        stats["topk"] = float(out.split("Top k time: ")[1].split("\n")[0])
        stats["total"] = float(out.split("Total time: ")[1].split("\n")[0])
        return stats
    except:
        print("Error")
        print("%s:\n%s\n%s" % (file_in,stdout.decode(),stderr.decode()))
        return {}




in_path = os.path.join('out','dDNNF')
compilation_stats_path = os.path.join("out","d4_compilation_stats")

for limit in [1000000]:
    for nb in [1000,10000,25000,50000,75000,100000,110000,120000,130000,140000,150000,160000,170000,180000,190000,200000]:
        stat_data_path = os.path.join("out","max_config_jddnnf")
        if not os.path.exists(stat_data_path):
            os.makedirs(stat_data_path)

        for filename in ["LargeAutomotive"]:
            print("Running for example %s with limit %d and nb %d" % (filename,limit,nb))
            ddnnf_path = os.path.join(in_path,"%s.ddnnf" % filename)
            cs_path = os.path.join(compilation_stats_path,"%s.json" % filename)
            dest = os.path.join(stat_data_path,"%s_%d.json" % (filename,nb))
            if os.path.exists(ddnnf_path) and os.path.exists(cs_path) and not os.path.exists(dest):
                nb_variables = load_dict(cs_path)["Number of variables"]
                data = {}       
                for ite in range(5):
                    data[ite] = enumOpt(ddnnf_path,nb_variables,nb,limit)
                    print(data[ite])
                save_dict(data, dest)
    

    
    
    
    
