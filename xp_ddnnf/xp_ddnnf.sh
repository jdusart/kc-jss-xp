#!/bin/sh

python compile_d4.py
python run_satisfiability.py
python run_sampling.py
python run_counting.py
python run_top10_config_random_jddnnf.py
python run_top10_value_random_jddnnf.py
python run_max_config_jddnnf.py
