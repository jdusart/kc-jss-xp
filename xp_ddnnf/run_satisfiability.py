#!/usr/bin/env python
# coding: utf-8

# # Multithread prototype

# In[1]:


import os
import subprocess
import time
import json
import math
import random
import argparse


def save_dict(dic,path,indent=4):
    with open(path, 'w') as fp:
        json.dump(dic, fp, indent=indent)

def load_dict(path):
    if os.path.exists(path):
        with open(path, 'r') as fp:
            return json.load(fp)
    return {}

def sample(file_in,nb_var,nb):
    timeStarted = time.time()
    process = subprocess.Popen(["java","-Xmx12228m","-cp","prog/WINSTON-0.1-jss-alpha.jar","xps.jss.ddnnf.SampleKUS",file_in,str(nb_var),str(nb)], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = process.communicate()
    try:
        out = stdout.decode()
        stats = {}
        stats["parsing"] = float(out.split("Parsing time: ")[1].split("\n")[0])
        stats["preparation"] = float(out.split("Preparation time: ")[1].split("\n")[0])
        stats["satisfiability"] = float(out.split("Sampling time: ")[1].split("\n")[0])
        stats["total"] = time.time()-timeStarted
        return stats
    except:
        print("Error")
        print("%s:\n%s\n%s" % (file_in,stdout.decode(),stderr.decode()))
        return {}

in_path = os.path.join('out','dDNNF')
compilation_stats_path = os.path.join("out","d4_compilation_stats")


stat_data_path = os.path.join("out","point_out_1")
if not os.path.exists(stat_data_path):
    os.makedirs(stat_data_path)

for filename in [x.split('.ddnnf')[0] for x in os.listdir(in_path)]:
    print("Running for example %s" % (filename,))
    ddnnf_path = os.path.join(in_path,"%s.ddnnf" % filename)
    cs_path = os.path.join(compilation_stats_path,"%s.json" % filename)
    dest = os.path.join(stat_data_path,"%s.json" % (filename))
    if os.path.exists(ddnnf_path) and os.path.exists(cs_path) and not os.path.exists(dest):
        data = {}
        nb_variables = load_dict(cs_path)["Number of variables"]
        for ite in range(5):
            data[ite] = sample(ddnnf_path,nb_variables,1)
        save_dict(data, dest)

stat_data_path = os.path.join("out","point_out_10")
if not os.path.exists(stat_data_path):
    os.makedirs(stat_data_path)

for filename in [x.split('.ddnnf')[0] for x in os.listdir(in_path)]:
    print("Running for example %s" % (filename,))
    ddnnf_path = os.path.join(in_path,"%s.ddnnf" % filename)
    cs_path = os.path.join(compilation_stats_path,"%s.json" % filename)
    dest = os.path.join(stat_data_path,"%s.json" % (filename))
    if os.path.exists(ddnnf_path) and os.path.exists(cs_path) and not os.path.exists(dest):
        data = {}
        nb_variables = load_dict(cs_path)["Number of variables"]
        for ite in range(5):
            data[ite] = sample(ddnnf_path,nb_variables,10)
        save_dict(data, dest)
        
        
