#!/usr/bin/env python
# coding: utf-8

# # Multithread prototype

# In[1]:


import os
import subprocess
import time
import json
import math
import random
import argparse


def save_dict(dic,path,indent=4):
    with open(path, 'w') as fp:
        json.dump(dic, fp, indent=indent)

def load_dict(path):
    if os.path.exists(path):
        with open(path, 'r') as fp:
            return json.load(fp)
    return {}

def count(file_in,nb_var,nb):
    timeStarted = time.time()
    process = subprocess.Popen(["java","-Xmx12228m","-cp","prog/WINSTON-0.1-jss-alpha.jar","xps.jss.ddnnf.Count",file_in,str(nb_var),str(nb)], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = process.communicate()
    try:
        out = stdout.decode()
        stats = {}
        stats["#models"] = out.split("Count: ")[1].split("\n")[0]
        stats["parsing"] = float(out.split("Parsing time: ")[1].split("\n")[0])
        stats["count"] = float(out.split("Count time: ")[1].split("\n")[0])
        stats["total"] = time.time()-timeStarted
        return stats
    except:
        print("Error")
        print("%s:\n%s\n%s" % (file_in,stdout.decode(),stderr.decode()))
        return {}

in_path = os.path.join('out','dDNNF')
compilation_stats_path = os.path.join("out","d4_compilation_stats")


stat_data_path = os.path.join("out","counting_stats")
if not os.path.exists(stat_data_path):
    os.makedirs(stat_data_path)

for filename in [x.split('.ddnnf')[0] for x in os.listdir(in_path)]:
    print("Running for example %s" % (filename,))
    ddnnf_path = os.path.join(in_path,"%s.ddnnf" % filename)
    cs_path = os.path.join(compilation_stats_path,"%s.json" % filename)
    dest = os.path.join(stat_data_path,"%s.json" % (filename))
    if os.path.exists(ddnnf_path) and os.path.exists(cs_path) and not os.path.exists(dest):
        nb_variables = load_dict(cs_path)["Number of variables"]
        data = count(ddnnf_path,nb_variables,1)
        save_dict(data, dest)
    
